
#
import pandas as pd
import pyresearch
import requests
import json



def checkdfhostnameagainstinventory(parent_df, hostnamefield, datefilter=None, datefilterfield=None, debug=False):
    #PUBLIC
    inventory_hosts = {}
    if datefilter is None and datefilterfield is None: 
        filtered_df = parent_df
    elif datefilter is not None and datefilterfield is not None:
        filtered_df = parent_df[parent_df[datefilterfield] >= datefilter]
    else:
        print("Either a datefilter or datefilterfield was provided without the other, we neee both the field name and the date to filter")
        return None

    checkhosts = list(set(filtered_df[hostnamefield].tolist()))
    hosts_dict = get_host_labels(checkhosts, debug)

    return hosts_dict


def workspacenamelookup(wsid, debug=False):
    myipy = get_ipython()
    myid = wsid.replace("w", "")
    sql = "select name from workspaces where workspaceId = %s" % myid
    myipy.run_line_magic(u'mysql', 'connect silent')
    myipy.run_line_magic(u'mysql', 'set mysql_display_df False')
    myipy.run_cell_magic(u'mysql', "", sql)
    myname = myipy.user_ns['prev_mysql']['name'].tolist()[0]
    myipy.run_line_magic(u'mysql', 'set mysql_display_df True')
    myipy.run_line_magic(u'mysql', 'disconnect')
    return myname

 


def get_host_labels(hosts_list, debug=False):
    #PUBLIC
    host_assets = {}
    for host in hosts_list:
        if debug:
            print("Retrieving labels for host " + host)
        r = requests.get("http://gi.vip.riskiq:10080/v1/assets/HOST/" + host + "?labels=w")
        labels = set([])
        if 'labels' in r.json().keys():
            for l in r.json()['labels']:
                labels.add(l['label'].split(".")[0])
            for label in labels:
                if label in host_assets.keys():
                    host_assets[label]["hosts"].append(host)
                else:
                    host_assets[label] = {"hosts": [host]}
    return host_assets


